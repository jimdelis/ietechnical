/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "Party", "objectService" : "IETPartyObjectService_Dimitris"};

    var setterFunctions = {
        Address: function(val, state) {
            context["field"] = "Address";
            context["metadata"] = (objectMetadata ? objectMetadata["Address"] : null);
            state['Address'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        AgeInYears: function(val, state) {
            context["field"] = "AgeInYears";
            context["metadata"] = (objectMetadata ? objectMetadata["AgeInYears"] : null);
            state['AgeInYears'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        ApplicationId: function(val, state) {
            context["field"] = "ApplicationId";
            context["metadata"] = (objectMetadata ? objectMetadata["ApplicationId"] : null);
            state['ApplicationId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        City: function(val, state) {
            context["field"] = "City";
            context["metadata"] = (objectMetadata ? objectMetadata["City"] : null);
            state['City'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        Country: function(val, state) {
            context["field"] = "Country";
            context["metadata"] = (objectMetadata ? objectMetadata["Country"] : null);
            state['Country'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        CountryCode: function(val, state) {
            context["field"] = "CountryCode";
            context["metadata"] = (objectMetadata ? objectMetadata["CountryCode"] : null);
            state['CountryCode'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        DOB: function(val, state) {
            context["field"] = "DOB";
            context["metadata"] = (objectMetadata ? objectMetadata["DOB"] : null);
            state['DOB'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        Email: function(val, state) {
            context["field"] = "Email";
            context["metadata"] = (objectMetadata ? objectMetadata["Email"] : null);
            state['Email'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        ExpiryOn: function(val, state) {
            context["field"] = "ExpiryOn";
            context["metadata"] = (objectMetadata ? objectMetadata["ExpiryOn"] : null);
            state['ExpiryOn'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        FirstName: function(val, state) {
            context["field"] = "FirstName";
            context["metadata"] = (objectMetadata ? objectMetadata["FirstName"] : null);
            state['FirstName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        IdentityId: function(val, state) {
            context["field"] = "IdentityId";
            context["metadata"] = (objectMetadata ? objectMetadata["IdentityId"] : null);
            state['IdentityId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        IdentityType: function(val, state) {
            context["field"] = "IdentityType";
            context["metadata"] = (objectMetadata ? objectMetadata["IdentityType"] : null);
            state['IdentityType'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        IdValidityDuration: function(val, state) {
            context["field"] = "IdValidityDuration";
            context["metadata"] = (objectMetadata ? objectMetadata["IdValidityDuration"] : null);
            state['IdValidityDuration'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        IssuedCountry: function(val, state) {
            context["field"] = "IssuedCountry";
            context["metadata"] = (objectMetadata ? objectMetadata["IssuedCountry"] : null);
            state['IssuedCountry'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        IssuedOn: function(val, state) {
            context["field"] = "IssuedOn";
            context["metadata"] = (objectMetadata ? objectMetadata["IssuedOn"] : null);
            state['IssuedOn'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        IssuedState: function(val, state) {
            context["field"] = "IssuedState";
            context["metadata"] = (objectMetadata ? objectMetadata["IssuedState"] : null);
            state['IssuedState'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        LastName: function(val, state) {
            context["field"] = "LastName";
            context["metadata"] = (objectMetadata ? objectMetadata["LastName"] : null);
            state['LastName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        MiddleName: function(val, state) {
            context["field"] = "MiddleName";
            context["metadata"] = (objectMetadata ? objectMetadata["MiddleName"] : null);
            state['MiddleName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        Occupation: function(val, state) {
            context["field"] = "Occupation";
            context["metadata"] = (objectMetadata ? objectMetadata["Occupation"] : null);
            state['Occupation'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        PhoneNumber: function(val, state) {
            context["field"] = "PhoneNumber";
            context["metadata"] = (objectMetadata ? objectMetadata["PhoneNumber"] : null);
            state['PhoneNumber'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        SecretPin: function(val, state) {
            context["field"] = "SecretPin";
            context["metadata"] = (objectMetadata ? objectMetadata["SecretPin"] : null);
            state['SecretPin'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        State: function(val, state) {
            context["field"] = "State";
            context["metadata"] = (objectMetadata ? objectMetadata["State"] : null);
            state['State'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        Status: function(val, state) {
            context["field"] = "Status";
            context["metadata"] = (objectMetadata ? objectMetadata["Status"] : null);
            state['Status'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        ZipCode: function(val, state) {
            context["field"] = "ZipCode";
            context["metadata"] = (objectMetadata ? objectMetadata["ZipCode"] : null);
            state['ZipCode'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        CreatedBy: function(val, state) {
            context["field"] = "CreatedBy";
            context["metadata"] = (objectMetadata ? objectMetadata["CreatedBy"] : null);
            state['CreatedBy'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        LastUpdatedBy: function(val, state) {
            context["field"] = "LastUpdatedBy";
            context["metadata"] = (objectMetadata ? objectMetadata["LastUpdatedBy"] : null);
            state['LastUpdatedBy'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        CreatedDateTime: function(val, state) {
            context["field"] = "CreatedDateTime";
            context["metadata"] = (objectMetadata ? objectMetadata["CreatedDateTime"] : null);
            state['CreatedDateTime'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        LastUpdatedDateTime: function(val, state) {
            context["field"] = "LastUpdatedDateTime";
            context["metadata"] = (objectMetadata ? objectMetadata["LastUpdatedDateTime"] : null);
            state['LastUpdatedDateTime'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        SoftDeleteFlag: function(val, state) {
            context["field"] = "SoftDeleteFlag";
            context["metadata"] = (objectMetadata ? objectMetadata["SoftDeleteFlag"] : null);
            state['SoftDeleteFlag'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };

    //Create the Model Class
    function Party(defaultValues) {
        var privateState = {};
        context["field"] = "Address";
        context["metadata"] = (objectMetadata ? objectMetadata["Address"] : null);
        privateState.Address = defaultValues ?
            (defaultValues["Address"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Address"], context) :
                null) :
            null;

        context["field"] = "AgeInYears";
        context["metadata"] = (objectMetadata ? objectMetadata["AgeInYears"] : null);
        privateState.AgeInYears = defaultValues ?
            (defaultValues["AgeInYears"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["AgeInYears"], context) :
                null) :
            null;

        context["field"] = "ApplicationId";
        context["metadata"] = (objectMetadata ? objectMetadata["ApplicationId"] : null);
        privateState.ApplicationId = defaultValues ?
            (defaultValues["ApplicationId"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["ApplicationId"], context) :
                null) :
            null;

        context["field"] = "City";
        context["metadata"] = (objectMetadata ? objectMetadata["City"] : null);
        privateState.City = defaultValues ?
            (defaultValues["City"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["City"], context) :
                null) :
            null;

        context["field"] = "Country";
        context["metadata"] = (objectMetadata ? objectMetadata["Country"] : null);
        privateState.Country = defaultValues ?
            (defaultValues["Country"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Country"], context) :
                null) :
            null;

        context["field"] = "CountryCode";
        context["metadata"] = (objectMetadata ? objectMetadata["CountryCode"] : null);
        privateState.CountryCode = defaultValues ?
            (defaultValues["CountryCode"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["CountryCode"], context) :
                null) :
            null;

        context["field"] = "DOB";
        context["metadata"] = (objectMetadata ? objectMetadata["DOB"] : null);
        privateState.DOB = defaultValues ?
            (defaultValues["DOB"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["DOB"], context) :
                null) :
            null;

        context["field"] = "Email";
        context["metadata"] = (objectMetadata ? objectMetadata["Email"] : null);
        privateState.Email = defaultValues ?
            (defaultValues["Email"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Email"], context) :
                null) :
            null;

        context["field"] = "ExpiryOn";
        context["metadata"] = (objectMetadata ? objectMetadata["ExpiryOn"] : null);
        privateState.ExpiryOn = defaultValues ?
            (defaultValues["ExpiryOn"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["ExpiryOn"], context) :
                null) :
            null;

        context["field"] = "FirstName";
        context["metadata"] = (objectMetadata ? objectMetadata["FirstName"] : null);
        privateState.FirstName = defaultValues ?
            (defaultValues["FirstName"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["FirstName"], context) :
                null) :
            null;

        context["field"] = "IdentityId";
        context["metadata"] = (objectMetadata ? objectMetadata["IdentityId"] : null);
        privateState.IdentityId = defaultValues ?
            (defaultValues["IdentityId"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["IdentityId"], context) :
                null) :
            null;

        context["field"] = "IdentityType";
        context["metadata"] = (objectMetadata ? objectMetadata["IdentityType"] : null);
        privateState.IdentityType = defaultValues ?
            (defaultValues["IdentityType"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["IdentityType"], context) :
                null) :
            null;

        context["field"] = "IdValidityDuration";
        context["metadata"] = (objectMetadata ? objectMetadata["IdValidityDuration"] : null);
        privateState.IdValidityDuration = defaultValues ?
            (defaultValues["IdValidityDuration"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["IdValidityDuration"], context) :
                null) :
            null;

        context["field"] = "IssuedCountry";
        context["metadata"] = (objectMetadata ? objectMetadata["IssuedCountry"] : null);
        privateState.IssuedCountry = defaultValues ?
            (defaultValues["IssuedCountry"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["IssuedCountry"], context) :
                null) :
            null;

        context["field"] = "IssuedOn";
        context["metadata"] = (objectMetadata ? objectMetadata["IssuedOn"] : null);
        privateState.IssuedOn = defaultValues ?
            (defaultValues["IssuedOn"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["IssuedOn"], context) :
                null) :
            null;

        context["field"] = "IssuedState";
        context["metadata"] = (objectMetadata ? objectMetadata["IssuedState"] : null);
        privateState.IssuedState = defaultValues ?
            (defaultValues["IssuedState"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["IssuedState"], context) :
                null) :
            null;

        context["field"] = "LastName";
        context["metadata"] = (objectMetadata ? objectMetadata["LastName"] : null);
        privateState.LastName = defaultValues ?
            (defaultValues["LastName"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["LastName"], context) :
                null) :
            null;

        context["field"] = "MiddleName";
        context["metadata"] = (objectMetadata ? objectMetadata["MiddleName"] : null);
        privateState.MiddleName = defaultValues ?
            (defaultValues["MiddleName"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["MiddleName"], context) :
                null) :
            null;

        context["field"] = "Occupation";
        context["metadata"] = (objectMetadata ? objectMetadata["Occupation"] : null);
        privateState.Occupation = defaultValues ?
            (defaultValues["Occupation"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Occupation"], context) :
                null) :
            null;

        context["field"] = "PhoneNumber";
        context["metadata"] = (objectMetadata ? objectMetadata["PhoneNumber"] : null);
        privateState.PhoneNumber = defaultValues ?
            (defaultValues["PhoneNumber"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["PhoneNumber"], context) :
                null) :
            null;

        context["field"] = "SecretPin";
        context["metadata"] = (objectMetadata ? objectMetadata["SecretPin"] : null);
        privateState.SecretPin = defaultValues ?
            (defaultValues["SecretPin"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["SecretPin"], context) :
                null) :
            null;

        context["field"] = "State";
        context["metadata"] = (objectMetadata ? objectMetadata["State"] : null);
        privateState.State = defaultValues ?
            (defaultValues["State"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["State"], context) :
                null) :
            null;

        context["field"] = "Status";
        context["metadata"] = (objectMetadata ? objectMetadata["Status"] : null);
        privateState.Status = defaultValues ?
            (defaultValues["Status"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["Status"], context) :
                null) :
            null;

        context["field"] = "ZipCode";
        context["metadata"] = (objectMetadata ? objectMetadata["ZipCode"] : null);
        privateState.ZipCode = defaultValues ?
            (defaultValues["ZipCode"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["ZipCode"], context) :
                null) :
            null;

        context["field"] = "CreatedBy";
        context["metadata"] = (objectMetadata ? objectMetadata["CreatedBy"] : null);
        privateState.CreatedBy = defaultValues ?
            (defaultValues["CreatedBy"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["CreatedBy"], context) :
                null) :
            null;

        context["field"] = "LastUpdatedBy";
        context["metadata"] = (objectMetadata ? objectMetadata["LastUpdatedBy"] : null);
        privateState.LastUpdatedBy = defaultValues ?
            (defaultValues["LastUpdatedBy"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["LastUpdatedBy"], context) :
                null) :
            null;

        context["field"] = "CreatedDateTime";
        context["metadata"] = (objectMetadata ? objectMetadata["CreatedDateTime"] : null);
        privateState.CreatedDateTime = defaultValues ?
            (defaultValues["CreatedDateTime"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["CreatedDateTime"], context) :
                null) :
            null;

        context["field"] = "LastUpdatedDateTime";
        context["metadata"] = (objectMetadata ? objectMetadata["LastUpdatedDateTime"] : null);
        privateState.LastUpdatedDateTime = defaultValues ?
            (defaultValues["LastUpdatedDateTime"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["LastUpdatedDateTime"], context) :
                null) :
            null;

        context["field"] = "SoftDeleteFlag";
        context["metadata"] = (objectMetadata ? objectMetadata["SoftDeleteFlag"] : null);
        privateState.SoftDeleteFlag = defaultValues ?
            (defaultValues["SoftDeleteFlag"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["SoftDeleteFlag"], context) :
                null) :
            null;


        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);

        //Defining Getter/Setters
        Object.defineProperties(this, {
            "Address": {
                get: function() {
                    context["field"] = "Address";
                    context["metadata"] = (objectMetadata ? objectMetadata["Address"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Address, context);
                },
                set: function(val) {
                    setterFunctions['Address'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "AgeInYears": {
                get: function() {
                    context["field"] = "AgeInYears";
                    context["metadata"] = (objectMetadata ? objectMetadata["AgeInYears"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.AgeInYears, context);
                },
                set: function(val) {
                    setterFunctions['AgeInYears'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "ApplicationId": {
                get: function() {
                    context["field"] = "ApplicationId";
                    context["metadata"] = (objectMetadata ? objectMetadata["ApplicationId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.ApplicationId, context);
                },
                set: function(val) {
                    setterFunctions['ApplicationId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "City": {
                get: function() {
                    context["field"] = "City";
                    context["metadata"] = (objectMetadata ? objectMetadata["City"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.City, context);
                },
                set: function(val) {
                    setterFunctions['City'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "Country": {
                get: function() {
                    context["field"] = "Country";
                    context["metadata"] = (objectMetadata ? objectMetadata["Country"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Country, context);
                },
                set: function(val) {
                    setterFunctions['Country'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "CountryCode": {
                get: function() {
                    context["field"] = "CountryCode";
                    context["metadata"] = (objectMetadata ? objectMetadata["CountryCode"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.CountryCode, context);
                },
                set: function(val) {
                    setterFunctions['CountryCode'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "DOB": {
                get: function() {
                    context["field"] = "DOB";
                    context["metadata"] = (objectMetadata ? objectMetadata["DOB"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.DOB, context);
                },
                set: function(val) {
                    setterFunctions['DOB'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "Email": {
                get: function() {
                    context["field"] = "Email";
                    context["metadata"] = (objectMetadata ? objectMetadata["Email"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Email, context);
                },
                set: function(val) {
                    setterFunctions['Email'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "ExpiryOn": {
                get: function() {
                    context["field"] = "ExpiryOn";
                    context["metadata"] = (objectMetadata ? objectMetadata["ExpiryOn"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.ExpiryOn, context);
                },
                set: function(val) {
                    setterFunctions['ExpiryOn'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "FirstName": {
                get: function() {
                    context["field"] = "FirstName";
                    context["metadata"] = (objectMetadata ? objectMetadata["FirstName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.FirstName, context);
                },
                set: function(val) {
                    setterFunctions['FirstName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "IdentityId": {
                get: function() {
                    context["field"] = "IdentityId";
                    context["metadata"] = (objectMetadata ? objectMetadata["IdentityId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.IdentityId, context);
                },
                set: function(val) {
                    setterFunctions['IdentityId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "IdentityType": {
                get: function() {
                    context["field"] = "IdentityType";
                    context["metadata"] = (objectMetadata ? objectMetadata["IdentityType"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.IdentityType, context);
                },
                set: function(val) {
                    setterFunctions['IdentityType'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "IdValidityDuration": {
                get: function() {
                    context["field"] = "IdValidityDuration";
                    context["metadata"] = (objectMetadata ? objectMetadata["IdValidityDuration"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.IdValidityDuration, context);
                },
                set: function(val) {
                    setterFunctions['IdValidityDuration'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "IssuedCountry": {
                get: function() {
                    context["field"] = "IssuedCountry";
                    context["metadata"] = (objectMetadata ? objectMetadata["IssuedCountry"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.IssuedCountry, context);
                },
                set: function(val) {
                    setterFunctions['IssuedCountry'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "IssuedOn": {
                get: function() {
                    context["field"] = "IssuedOn";
                    context["metadata"] = (objectMetadata ? objectMetadata["IssuedOn"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.IssuedOn, context);
                },
                set: function(val) {
                    setterFunctions['IssuedOn'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "IssuedState": {
                get: function() {
                    context["field"] = "IssuedState";
                    context["metadata"] = (objectMetadata ? objectMetadata["IssuedState"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.IssuedState, context);
                },
                set: function(val) {
                    setterFunctions['IssuedState'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "LastName": {
                get: function() {
                    context["field"] = "LastName";
                    context["metadata"] = (objectMetadata ? objectMetadata["LastName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.LastName, context);
                },
                set: function(val) {
                    setterFunctions['LastName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "MiddleName": {
                get: function() {
                    context["field"] = "MiddleName";
                    context["metadata"] = (objectMetadata ? objectMetadata["MiddleName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.MiddleName, context);
                },
                set: function(val) {
                    setterFunctions['MiddleName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "Occupation": {
                get: function() {
                    context["field"] = "Occupation";
                    context["metadata"] = (objectMetadata ? objectMetadata["Occupation"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Occupation, context);
                },
                set: function(val) {
                    setterFunctions['Occupation'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "PhoneNumber": {
                get: function() {
                    context["field"] = "PhoneNumber";
                    context["metadata"] = (objectMetadata ? objectMetadata["PhoneNumber"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.PhoneNumber, context);
                },
                set: function(val) {
                    setterFunctions['PhoneNumber'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "SecretPin": {
                get: function() {
                    context["field"] = "SecretPin";
                    context["metadata"] = (objectMetadata ? objectMetadata["SecretPin"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.SecretPin, context);
                },
                set: function(val) {
                    setterFunctions['SecretPin'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "State": {
                get: function() {
                    context["field"] = "State";
                    context["metadata"] = (objectMetadata ? objectMetadata["State"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.State, context);
                },
                set: function(val) {
                    setterFunctions['State'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "Status": {
                get: function() {
                    context["field"] = "Status";
                    context["metadata"] = (objectMetadata ? objectMetadata["Status"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.Status, context);
                },
                set: function(val) {
                    setterFunctions['Status'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "ZipCode": {
                get: function() {
                    context["field"] = "ZipCode";
                    context["metadata"] = (objectMetadata ? objectMetadata["ZipCode"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.ZipCode, context);
                },
                set: function(val) {
                    setterFunctions['ZipCode'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "CreatedBy": {
                get: function() {
                    context["field"] = "CreatedBy";
                    context["metadata"] = (objectMetadata ? objectMetadata["CreatedBy"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.CreatedBy, context);
                },
                set: function(val) {
                    setterFunctions['CreatedBy'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "LastUpdatedBy": {
                get: function() {
                    context["field"] = "LastUpdatedBy";
                    context["metadata"] = (objectMetadata ? objectMetadata["LastUpdatedBy"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.LastUpdatedBy, context);
                },
                set: function(val) {
                    setterFunctions['LastUpdatedBy'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "CreatedDateTime": {
                get: function() {
                    context["field"] = "CreatedDateTime";
                    context["metadata"] = (objectMetadata ? objectMetadata["CreatedDateTime"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.CreatedDateTime, context);
                },
                set: function(val) {
                    setterFunctions['CreatedDateTime'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "LastUpdatedDateTime": {
                get: function() {
                    context["field"] = "LastUpdatedDateTime";
                    context["metadata"] = (objectMetadata ? objectMetadata["LastUpdatedDateTime"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.LastUpdatedDateTime, context);
                },
                set: function(val) {
                    setterFunctions['LastUpdatedDateTime'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "SoftDeleteFlag": {
                get: function() {
                    context["field"] = "SoftDeleteFlag";
                    context["metadata"] = (objectMetadata ? objectMetadata["SoftDeleteFlag"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.SoftDeleteFlag, context);
                },
                set: function(val) {
                    setterFunctions['SoftDeleteFlag'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });

        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };

        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.Address = value ? (value["Address"] ? value["Address"] : null) : null;
            privateState.AgeInYears = value ? (value["AgeInYears"] ? value["AgeInYears"] : null) : null;
            privateState.ApplicationId = value ? (value["ApplicationId"] ? value["ApplicationId"] : null) : null;
            privateState.City = value ? (value["City"] ? value["City"] : null) : null;
            privateState.Country = value ? (value["Country"] ? value["Country"] : null) : null;
            privateState.CountryCode = value ? (value["CountryCode"] ? value["CountryCode"] : null) : null;
            privateState.DOB = value ? (value["DOB"] ? value["DOB"] : null) : null;
            privateState.Email = value ? (value["Email"] ? value["Email"] : null) : null;
            privateState.ExpiryOn = value ? (value["ExpiryOn"] ? value["ExpiryOn"] : null) : null;
            privateState.FirstName = value ? (value["FirstName"] ? value["FirstName"] : null) : null;
            privateState.IdentityId = value ? (value["IdentityId"] ? value["IdentityId"] : null) : null;
            privateState.IdentityType = value ? (value["IdentityType"] ? value["IdentityType"] : null) : null;
            privateState.IdValidityDuration = value ? (value["IdValidityDuration"] ? value["IdValidityDuration"] : null) : null;
            privateState.IssuedCountry = value ? (value["IssuedCountry"] ? value["IssuedCountry"] : null) : null;
            privateState.IssuedOn = value ? (value["IssuedOn"] ? value["IssuedOn"] : null) : null;
            privateState.IssuedState = value ? (value["IssuedState"] ? value["IssuedState"] : null) : null;
            privateState.LastName = value ? (value["LastName"] ? value["LastName"] : null) : null;
            privateState.MiddleName = value ? (value["MiddleName"] ? value["MiddleName"] : null) : null;
            privateState.Occupation = value ? (value["Occupation"] ? value["Occupation"] : null) : null;
            privateState.PhoneNumber = value ? (value["PhoneNumber"] ? value["PhoneNumber"] : null) : null;
            privateState.SecretPin = value ? (value["SecretPin"] ? value["SecretPin"] : null) : null;
            privateState.State = value ? (value["State"] ? value["State"] : null) : null;
            privateState.Status = value ? (value["Status"] ? value["Status"] : null) : null;
            privateState.ZipCode = value ? (value["ZipCode"] ? value["ZipCode"] : null) : null;
            privateState.CreatedBy = value ? (value["CreatedBy"] ? value["CreatedBy"] : null) : null;
            privateState.LastUpdatedBy = value ? (value["LastUpdatedBy"] ? value["LastUpdatedBy"] : null) : null;
            privateState.CreatedDateTime = value ? (value["CreatedDateTime"] ? value["CreatedDateTime"] : null) : null;
            privateState.LastUpdatedDateTime = value ? (value["LastUpdatedDateTime"] ? value["LastUpdatedDateTime"] : null) : null;
            privateState.SoftDeleteFlag = value ? (value["SoftDeleteFlag"] ? value["SoftDeleteFlag"] : null) : null;
        };
    }

    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(Party);

    //Create new class level validator object
    BaseModel.Validator.call(Party);

    var registerValidatorBackup = Party.registerValidator;

    Party.registerValidator = function() {
        var propName = arguments[0];
        if(!setterFunctions[propName].changed) {
            var setterBackup = setterFunctions[propName];
            setterFunctions[arguments[0]] = function() {
                if(Party.isValid(this, propName, val)) {
                    return setterBackup.apply(null, arguments);
                } else {
                    throw Error("Validation failed for " + propName + " : " + val);
                }
            }
            setterFunctions[arguments[0]].changed = true;
        }
        return registerValidatorBackup.apply(null, arguments);
    }

    //Extending Model for custom operations
    var relations = [];

    Party.relations = relations;

    Party.prototype.isValid = function() {
        return Party.isValid(this);
    };

    Party.prototype.objModelName = "Party";

    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    Party.registerProcessors = function(options, successCallback, failureCallback) {

        if(!options) {
            options = {};
        }

        if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }

        if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }

        kony.mvc.util.ProcessorUtils.getMetadataForObject("IETPartyObjectService_Dimitris", "Party", options, metaDataSuccess, metaDataFailure);
    };

    //clone the object provided in argument.
    Party.clone = function(objectToClone) {
        var clonedObj = new Party();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };

    return Party;
});