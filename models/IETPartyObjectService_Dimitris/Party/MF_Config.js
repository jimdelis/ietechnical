/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"Address": "Address",
		"AgeInYears": "AgeInYears",
		"ApplicationId": "ApplicationId",
		"City": "City",
		"Country": "Country",
		"CountryCode": "CountryCode",
		"DOB": "DOB",
		"Email": "Email",
		"ExpiryOn": "ExpiryOn",
		"FirstName": "FirstName",
		"IdentityId": "IdentityId",
		"IdentityType": "IdentityType",
		"IdValidityDuration": "IdValidityDuration",
		"IssuedCountry": "IssuedCountry",
		"IssuedOn": "IssuedOn",
		"IssuedState": "IssuedState",
		"LastName": "LastName",
		"MiddleName": "MiddleName",
		"Occupation": "Occupation",
		"PhoneNumber": "PhoneNumber",
		"SecretPin": "SecretPin",
		"State": "State",
		"Status": "Status",
		"ZipCode": "ZipCode",
		"CreatedBy": "CreatedBy",
		"LastUpdatedBy": "LastUpdatedBy",
		"CreatedDateTime": "CreatedDateTime",
		"LastUpdatedDateTime": "LastUpdatedDateTime",
		"SoftDeleteFlag": "SoftDeleteFlag",
	};

	Object.freeze(mappings);

	var typings = {
		"Address": "string",
		"AgeInYears": "string",
		"ApplicationId": "number",
		"City": "string",
		"Country": "string",
		"CountryCode": "string",
		"DOB": "string",
		"Email": "string",
		"ExpiryOn": "string",
		"FirstName": "string",
		"IdentityId": "string",
		"IdentityType": "string",
		"IdValidityDuration": "string",
		"IssuedCountry": "string",
		"IssuedOn": "string",
		"IssuedState": "string",
		"LastName": "string",
		"MiddleName": "string",
		"Occupation": "string",
		"PhoneNumber": "string",
		"SecretPin": "string",
		"State": "string",
		"Status": "string",
		"ZipCode": "string",
		"CreatedBy": "string",
		"LastUpdatedBy": "string",
		"CreatedDateTime": "date",
		"LastUpdatedDateTime": "date",
		"SoftDeleteFlag": "boolean",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"ApplicationId",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "IETPartyObjectService_Dimitris",
		tableName: "Party"
	};

	Object.freeze(config);

	return config;
})