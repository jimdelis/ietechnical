/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function(){
	var repoMapping = {
		Party  : {
			model : "IETPartyObjectService_Dimitris/Party/Model",
			config : "IETPartyObjectService_Dimitris/Party/MF_Config",
            repository : "",
		},
		PartyMS  : {
			model : "PartyObjectMS_Dimitris/PartyMS/Model",
			config : "PartyObjectMS_Dimitris/PartyMS/MF_Config",
			repository : "PartyObjectMS_Dimitris/PartyMS/Repository",
		},
	};

	return repoMapping;
})