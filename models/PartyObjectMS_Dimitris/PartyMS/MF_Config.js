/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"dateOfBirth": "dateOfBirth",
		"firstName": "firstName",
		"lastName": "lastName",
		"electronicAddress": "electronicAddress",
		"id": "id",
	};

	Object.freeze(mappings);

	var typings = {
		"dateOfBirth": "string",
		"firstName": "string",
		"lastName": "string",
		"electronicAddress": "string",
		"id": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"id",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "PartyObjectMS_Dimitris",
		tableName: "PartyMS"
	};

	Object.freeze(config);

	return config;
})