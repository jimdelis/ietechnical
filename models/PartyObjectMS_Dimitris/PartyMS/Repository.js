define([], function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;

	//Create the Repository Class
	function PartyMSRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};

	//Setting BaseRepository as Parent to this Repository
	PartyMSRepository.prototype = Object.create(BaseRepository.prototype);
	PartyMSRepository.prototype.constructor = PartyMSRepository;

	//For Operation 'createPartyMS' with service id 'createParty5485'
	PartyMSRepository.prototype.createPartyMS = function(params, onCompletion){
		return PartyMSRepository.prototype.customVerb('createPartyMS', params, onCompletion);
	};

	return PartyMSRepository;
})