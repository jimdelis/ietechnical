define([], function () { 

  /**
     * User defined business controller
     * @constructor
     * @extends kony.mvc.Business.Delegator
     */
  function Onboarding_BusinessController() { 

    kony.mvc.Business.Delegator.call(this); 

  } 
  inheritsFrom(Onboarding_BusinessController, kony.mvc.Business.Delegator); 

  /*
     * This method invokes the Integration service call in the fabric to fetch the countries list.
     */
  Onboarding_BusinessController.prototype.getCountryList = function(presentationSuccessCallback, presentationErrorCallback) {

    //Call the configured IntegrationService operation to fetch the countries list
    var integrationObject = kony.sdk.getCurrentInstance().getIntegrationService("IETCountriesService_Dimitris");
    //My Change - How to pass the apikey and page_size from Business Controller to the service
    //integrationObject.invokeOperation("getCountries", {"apikey":"ALfYKlkV31xxTHmvAozZeC0vGehDkuxU"}, {"page_size":2000},presentationSC, presentationEC);

    integrationObject.invokeOperation("getCountries", {"apikey":"7AoPAZy4xxOAPc2IpiNGEP2JVLtSyelW"}, {}, presentationSC, presentationEC);

    // If the service provides countries list response, pass it to the Successcallback
    function presentationSC(response) {
      alert("response   " + JSON.stringify(response));
      presentationSuccessCallback(response);
    }
    // If the service provides any error  pass it to the Error callback
    function presentationEC(error) {
      alert("error   " + JSON.stringify(error));
      presentationErrorCallback(error);
    }
  };


  /*
* It is used to save the collected details by invoking the required Object Service. The first parameter in this function contains the collected inputs from user. 
*/

  Onboarding_BusinessController.prototype.addDetailsToStorage = function(partyObjectFieldValues,presentationSuccessCallback,presentationErrorCallback){      
    //Connect to the required data model(Object)
    var objAddDetails = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Party");    	  
    kony.print("Final Object Values"+JSON.stringify(partyObjectFieldValues));
    objAddDetails.save(partyObjectFieldValues, addDetailsToStorageCompletionCallback);
    function addDetailsToStorageCompletionCallback(status, data, error) {         
      var srh = applicationManager.getServiceResponseHandler();       
      var obj = srh.manageResponse(status, data, error); // Generalizes the service response in uniform format        
      kony.print("Data from addDetailsToStorageCompletionCallback ... "+JSON.stringify(obj));         
      if (obj["status"] === true) {   // invocation is Service is successful and received response         
        presentationSuccessCallback(obj["data"]); // pass the response to the Success callback       
      } else {            
        presentationErrorCallback(obj["errmsg"]);  // pass the error to the Error callback           
      }      
    }   
  };



  return Onboarding_BusinessController;

});