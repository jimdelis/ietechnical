define({
  getOccupationList : function(presentationSuccessCallback,presentationErrorCallback){      

    //Make sure the Integration service name matches with your service name
    var integrationObject = kony.sdk.getCurrentInstance().getIntegrationService("IETJavaService_Dimitris");
    integrationObject.invokeOperation("getOccupationList", {}, {},presentationSC, presentationEC);
    function presentationSC(response){
      //alert("response   "+JSON.stringify(response));
      presentationSuccessCallback(response);
    }
    function presentationEC(error){
      //alert("error   "+JSON.stringify(error));
      presentationErrorCallback(error);
    }
  },
  
    addDetailsToStorage : function(partyObjectFieldValues,presentationSuccessCallback,presentationErrorCallback){

    var objAddDetails = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("PartyMS");
  //  var objAddDetails = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Party");

    kony.print("Final Object Values"+JSON.stringify(partyObjectFieldValues));
    var partyDetails = {"firstName":partyObjectFieldValues.FirstName,"lastName":partyObjectFieldValues.LastName,"dateOfBirth":partyObjectFieldValues.DOB};
    //objAddDetails.save(partyObjectFieldValues, addDetailsToStorageCompletionCallback);
    objAddDetails.customVerb('createPartyMS', partyDetails, addDetailsToStorageCompletionCallback);

    function addDetailsToStorageCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      kony.print("Data from addDetailsToStorageCompletionCallback ... "+JSON.stringify(obj));
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      } else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  }
});