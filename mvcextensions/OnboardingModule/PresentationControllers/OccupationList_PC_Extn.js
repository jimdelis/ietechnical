define({
  getOccupationList : function() {
    var onboardingObj = applicationManager.getOnboardingManager();
    onboardingObj.getOccupationList(this.getOccupationListSC.bind(this),this.getOccupationListEC.bind(this));     

  },

  getOccupationListSC : function(response){  
    var occupationListGroupedIntoSections=[];  // [[{"occupation_type":"Private"},privateOccupationListResponse],[        ]]

    var privateOccupationListResponse = response.Private;
    if (null !== privateOccupationListResponse && undefined !== privateOccupationListResponse && privateOccupationListResponse.length > 0) {
      var dataForPrivateOccupationListSection=[{"occupation_type":"Private"},privateOccupationListResponse];
      occupationListGroupedIntoSections.push(dataForPrivateOccupationListSection);
    }

    var publicOccupationListResponse = response.Public;
    if (null !== publicOccupationListResponse && undefined !== publicOccupationListResponse && publicOccupationListResponse.length > 0) {
      var dataForPublicOccupationListResponse=[{"occupation_type":"Public"},publicOccupationListResponse];
      occupationListGroupedIntoSections.push(dataForPublicOccupationListResponse);
    }

    presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.dismissLoadingScreen();

    var navigationObj = applicationManager.getNavigationManager();  
    navigationObj.updateForm({"occupationListGroupedIntoSections":occupationListGroupedIntoSections},"frmPartyPersonalDetails"); 
  },


  getOccupationListEC : function(errMsg){  

    presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.dismissLoadingScreen();

    alert("Error Fetching Occupation List ###### "+JSON.stringify(errMsg));    

    var navigationObj = applicationManager.getNavigationManager();  
    navigationObj.updateForm({"occupationListErrorMessage":errMsg},"frmPartyPersonalDetails");     

  },

  addOnboardingSC : function(response){

    //alert("Details Added Sucessfully #### . "+JSON.stringify(response));
    var navigationObj = applicationManager.getNavigationManager();

    //var applicationStatus = {"response": "Success"};
    //navigationObj.setCustomInfo("ApplicationStatus",applicationStatus);
    //navigationObj.navigateTo("OnboardingModule/frmAcknowledgement", response);
    kony.print("Create Party Success Callback extension -> "+JSON.stringify(response));
    presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.dismissLoadingScreen();
    navigationObj.updateForm({"applicationSubmitResponse":response},"OnboardingModule/frmAcknowledgement");
    navigationObj.navigateTo("OnboardingModule/frmAcknowledgement");

  },
});