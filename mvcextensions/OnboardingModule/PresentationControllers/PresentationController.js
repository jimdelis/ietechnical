define([], function() {
  /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
  function Onboarding_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(Onboarding_PresentationController, kony.mvc.Presentation.BasePresenter);

  /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
  Onboarding_PresentationController.prototype.initializePresentationController = function() {

  };

  /*
      * This Success callback collects the passed response. 
      It converts the data to listbox master data acceptable form. i.e it converts the data to array of arrays 
      For later use, set the converted data as a value object using Navigation Manager utility function
      */
  Onboarding_PresentationController.prototype.getCountryListSC = function(response) {

    //alert("Country List from Temenos API Service #### . "+JSON.stringify(response.body));
    var countryArray = response.body;

    var resList = [];
    try {
      var data = countryArray;
      var length = data.length;
      var resObj = [];
      for (var i = 0; i < length; i++) {
        resObj = [ //[
          data[i]["countryId"], // "AD" ,
          data[i]["displayName"] // "Andorra"
        ]; // ]
        resList.push(resObj); //[ ["AD" , "Andorra"],]
      }
    } catch (exp) {
      kony.print("error");
    }
    var listData = {
      "countryListData": resList
    };
    var navigationObj = applicationManager.getNavigationManager();
    navigationObj.setCustomInfo("CountryList", listData);
    navigationObj.navigateTo("OnboardingModule/frmPartyAddressDetails");
  };

  /*
     * This Error callback dismisses the loadingscreen and set the error object using NavigationManager for later use
     */
  Onboarding_PresentationController.prototype.getCountryListEC = function(errMsg) {
    presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.dismissLoadingScreen();
    alert("Error Fetching Country List ###### " + JSON.stringify(errMsg));
    var navigationObj = applicationManager.getNavigationManager();
    navigationObj.setCustomInfo("countryListerror", errMsg);
  };


  /* Convert the data attributes of the received data in such a format that fabric service actually accepts. Set the converted data as value object using NavigationManager for later use. Get the Onboarding manager Business Controller using Application Manager and invoke the method. 
     *
     */
  Onboarding_PresentationController.prototype.processPersonalDetails = function(personalDetails) {
    //Convert the data into fabric Integration service acceptable format
    var partyObjectFieldValues = {
      "FirstName": personalDetails.firstName,
      "MiddleName": personalDetails.middleName,
      "LastName": personalDetails.lastName,
      "DOB": personalDetails.dob,
      "Email": personalDetails.email,
      "CountryCode": personalDetails.countryCode,
      "PhoneNumber": personalDetails.phoneNumber,
      "Occupation": personalDetails.occupation,
      "AgeInYears": personalDetails.ageInYears,

    };

    var navigationObj = applicationManager.getNavigationManager();
    navigationObj.setCustomInfo("PersonalDetails", partyObjectFieldValues);

    //Get the Onboarding manager Business Controller using Application Manager and invoke the method.
    var onboardingManager = applicationManager.getOnboardingManager();
    onboardingManager.getCountryList(this.getCountryListSC.bind(this), this.getCountryListEC.bind(this));

  };

  /*
  * This method receives the Address details passed as parameter. It creates an object with cumulative details of Personal and address. Set the object using Navigation Manager for later use.
  */
  Onboarding_PresentationController.prototype.processAddressDetails = function(addressDetails) {

    var navigationObj = applicationManager.getNavigationManager();  
    var partyObjectFieldValues = navigationObj.getCustomInfo("PersonalDetails");

    partyObjectFieldValues.Address =  addressDetails.address;      
    partyObjectFieldValues.Country = addressDetails.country ;     
    partyObjectFieldValues.State = addressDetails.state;  
    partyObjectFieldValues.City = addressDetails.city;  
    partyObjectFieldValues.ZipCode = addressDetails.zipCode;

    navigationObj.setCustomInfo("PersonalAndAddressDetails",partyObjectFieldValues);  
    navigationObj.navigateTo("OnboardingModule/frmPartyIdentityDetails"); 

  };

  /* This method receives the Identiy details passed as parameter. Get the cumulative details of Personal and Address details and append the identity details to the same object.  Set the object with cumulative details using Navigation Manager for later use.
    */
  Onboarding_PresentationController.prototype.processIdentityDetails = function(identityDetails) {


    var navigationObj = applicationManager.getNavigationManager();  
    var partyObjectFieldValues = navigationObj.getCustomInfo("PersonalAndAddressDetails");

    partyObjectFieldValues.IdentityType =  identityDetails.identityType;      
    partyObjectFieldValues.IdentityId = identityDetails.identiyId ;     
    partyObjectFieldValues.IssuedCountry = identityDetails.issuedCountry;  
    partyObjectFieldValues.IssuedState = identityDetails.issuedState; 
    partyObjectFieldValues.IssuedOn = identityDetails.issuedOn; 
    partyObjectFieldValues.ExpiryOn = identityDetails.expiryOn;
    partyObjectFieldValues.SecretPin = identityDetails.secretPin;
    partyObjectFieldValues.IdValidityDuration = identityDetails.idValidityDuration;
    partyObjectFieldValues.Status = "Submitted";

    navigationObj.setCustomInfo("AllDetails",partyObjectFieldValues);  
    navigationObj.navigateTo("OnboardingModule/frmTermsAndConditions"); 

  };

  /*
* This method receives all the inputs entered by user using NavigationManager. It invokes the Businesscontroller method to save them using fabric
*/
  Onboarding_PresentationController.prototype.submitDetails = function() {

    var navigationObj = applicationManager.getNavigationManager();  
    var partyObjectFieldValues = navigationObj.getCustomInfo("AllDetails");

    var onboardingModuleObj = applicationManager.getOnboardingManager();
    onboardingModuleObj.addDetailsToStorage(partyObjectFieldValues,this.addOnboardingSC.bind(this),this.addOnboardingEC.bind(this)); 

  };


  /*
* This success callback sets the application status as Success and navigates you to Acknowledgement form
*/
  Onboarding_PresentationController.prototype.addOnboardingSC = function(response){  

    alert("Details Added Sucessfully #### . "+JSON.stringify(response));  	
    var navigationObj = applicationManager.getNavigationManager(); 
    var applicationStatus = {"response": "Success"};
    navigationObj.setCustomInfo("ApplicationStatus",applicationStatus);  
    navigationObj.navigateTo("OnboardingModule/frmAcknowledgement");
  }; 
  /*
* This error callback sets the application status as Error and navigates you to Acknowledgement form
*/
  Onboarding_PresentationController.prototype.addOnboardingEC = function(errMsg){  

    alert("Error Added Details Record ###### "+JSON.stringify(errMsg));  
    var navigationObj = applicationManager.getNavigationManager();  
    var applicationStatus = {"response": "Error"};
    navigationObj.setCustomInfo("ApplicationStatus",applicationStatus); 
    navigationObj.navigateTo("OnboardingModule/frmAcknowledgement");
  };


  return Onboarding_PresentationController;
});