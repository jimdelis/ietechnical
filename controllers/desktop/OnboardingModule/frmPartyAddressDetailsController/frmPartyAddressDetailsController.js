define({ 

  /*
  Invoke this method in frmPartyAddressDetails form's Preshow(). 
  * Access the Country list using NavigationManager and map it to the corresponding listbox
  */
  populateCountryList: function() {
    var presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.dismissLoadingScreen();
    var navigationObj = applicationManager.getNavigationManager();
    var countryList = navigationObj.getCustomInfo("CountryList");
    this.view.lstCountry.masterData = countryList.countryListData;
    //My Change
    this.view.lstCountry.selectedKey = "AU";
  },

  /* Invoke this method onclick of Next button.
  *	It collects the inputs from the frmPartyAddressDetails and pass it to the Presentation Controller
  */
  processAddressDetails : function(){  

    var addressDetails = {};
    addressDetails.address = this.view.tbxAddressLine.text;
    addressDetails.country = this.view.lstCountry.selectedKeyValue[1];
    addressDetails.state = this.view.tbxState.text; 
    addressDetails.city = this.view.tbxCity.text; 
    addressDetails.zipCode = this.view.tbxZipCode.text; 

    presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.showLoadingScreen();
    onboardingModule = applicationManager.getModule("OnboardingModule");  	
    onboardingModule.presentationController.processAddressDetails(addressDetails); 

  },


  autoFillFormData : function(){
    this.view.tbxAddressLine.text = "Miyapur";
    this.view.tbxState.text = "Telangana";
    this.view.tbxCity.text = "Hyderabad";
    this.view.tbxZipCode.text = "500000";
  }

});