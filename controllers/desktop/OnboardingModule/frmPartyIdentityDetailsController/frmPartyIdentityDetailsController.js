define({ 

  /*
Invoke this method in frmPartyIdentityDetails form's Preshow(). 
* Access the Country list using NavigationManager and map it to the corresponding listbox
*/
  populateCountryList : function(){
    presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.dismissLoadingScreen();
    var navigationObj = applicationManager.getNavigationManager();  
    var countryList = navigationObj.getCustomInfo("CountryList");  
    this.view.lstIdentityDocumentIssuedCountry.masterData = countryList.countryListData;
  },

  /*
  * This method helps in validating the Issue and expiry dates in frmPartyIdentityDetails form
  */
  validateDateRange : function(){

    var issDateYear = this.view.calIdentityDocumentIssuedDate.year;
    var issDateMonth = this.view.calIdentityDocumentIssuedDate.month;
    var issDateDay = this.view.calIdentityDocumentIssuedDate.day;


    var expDateYear = this.view.calIdentityDocumentExpiryDate.year;
    var expDateMonth = this.view.calIdentityDocumentExpiryDate.month;
    var expDateDay = this.view.calIdentityDocumentExpiryDate.day;

    if (issDateYear === expDateYear) {// 2000 // 2000
      if (expDateMonth < issDateMonth) {// 12 //11
        this.view.lblInvalidDateRange.text = kony.i18n.getLocalizedString('frmPartyIdentityDetails.lblInvalidDateRange.text.1');
        this.view.lblInvalidDateRange.isVisible = true;
        return false;
      } else if (issDateMonth === expDateMonth) {
        if ( expDateDay <= issDateDay) 
          this.view.lblInvalidDateRange.text = kony.i18n.getLocalizedString('frmPartyIdentityDetails.lblInvalidDateRange.text.1');
        this.view.lblInvalidDateRange.isVisible = true;
        return false;
      }
    }else if (expDateYear < issDateYear) {
      this.view.lblInvalidDateRange.text = kony.i18n.getLocalizedString('frmPartyIdentityDetails.lblInvalidDateRange.text.1');
      this.view.lblInvalidDateRange.isVisible = true;
      return false;
    } else{
      return true;
    }
  },
  /*
  * This method helps to find out the number of days between 
  */
  calculateDays : function(){
    var issuedDate = this.view.calIdentityDocumentIssuedDate.formattedDate;
    var expiryDate = this.view.calIdentityDocumentExpiryDate.formattedDate;

    var dateFirst = new Date(issuedDate);
    var dateSecond = new Date(expiryDate);
    // time difference
    var timeDiff = Math.abs(dateSecond.getTime() - dateFirst.getTime());

    // days difference
    var diffDays = Math.ceil(timeDiff / (1000 * 60 * 60 * 24));

    // difference
    return diffDays;
  },

  /*	
  * Invoke this method on onClick event of Next button.
  * It is used to collect the inputs and pass it to the presentation controller
  */
  processIdentityDetails : function(){  

    var dateRangeValue = this.validateDateRange();
    if(dateRangeValue){

      var noOfDays = this.calculateDays(); //This noOfDays is an extra field you are passing along with the collected data
      var IdentityDetails = {};
      IdentityDetails.identityType = this.view.lstIdentityDocumentType.selectedKeyValue[1];
      IdentityDetails.identiyId = this.view.tbxIdentityDocumentID.text;
      IdentityDetails.issuedCountry = this.view.lstIdentityDocumentIssuedCountry.selectedKeyValue[1];
      IdentityDetails.issuedState = this.view.tbxIdentityDocumentIssuedState.text; 
      IdentityDetails.issuedOn = this.view.calIdentityDocumentIssuedDate.formattedDate;
      IdentityDetails.expiryOn = this.view.calIdentityDocumentExpiryDate.formattedDate;
      IdentityDetails.secretPin = this.view.tbxPartySecretPin.text; 
      IdentityDetails.idValidityDuration = noOfDays; 

      onBoardingModule = applicationManager.getModule("OnboardingModule");  	
      onBoardingModule.presentationController.processIdentityDetails(IdentityDetails); 
    }
    else{
      alert("Please choose proper dates");
    }
  },

  autoFillFormData : function(){
    this.view.lstIdentityDocumentType.selectedKey = "PP";
    this.view.tbxIdentityDocumentID.text = "ASDSFSGSFG";
    this.view.lstIdentityDocumentIssuedCountry.selectedKey = "AU";
    this.view.tbxIdentityDocumentIssuedState.text = "Telangana";
    this.view.calIdentityDocumentIssuedDate.dateComponents = ["12", "12", "2017"];
    this.view.calIdentityDocumentExpiryDate.dateComponents = ["11", "12", "2027"];
    this.view.tbxPartySecretPin.text = "1234";
  },
  
    /*
  Invoke this method on the onTouchEnd event of eye image icon
  * This method helps you to mask or unmask the secure pin entered
  */
  checkSecurePinVisibility : function (){
    var tbxMaskingProperty = this.view.tbxPartySecretPin.secureTextEntry;
    //alert("tbxMaskingProperty *** "+ tbxMaskingProperty);
    if(tbxMaskingProperty){
      this.view.tbxPartySecretPin.secureTextEntry = false;
    }else{
      this.view.tbxPartySecretPin.secureTextEntry = true;
    }   
  },
  /* Invoke this method on onTouchEnd event of IssuedDate and ExpiryDate calendar widgets
  * This method helps to hide the error label displayed against dates selection.
  */
  hideErrorDisplay : function(){
    if(this.view.lblInvalidDateRange.isVisible === true){
      this.view.lblInvalidDateRange.isVisible = false;
    }
  },

});