define({ 

  /* Invoke this method in frmAcknolwdement form's Preshow(). 
* It fetches the ApplicationStatus using NavigationManager. Based on the application status , assign the corresponding message to Acknowledgment label
*/

  /* ******************* You have to comment this function invokation from form preshow when you are going through the MS/T24 flow*/
  appstatus : function(){  

    presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.dismissLoadingScreen();

    var navigationObj = applicationManager.getNavigationManager();  
    var applicationStatus = navigationObj.getCustomInfo("ApplicationStatus");
    if(applicationStatus.response === "Success"){
      this.view.lblAck.text = "Application Submitted Successfully";     
    } else {
      this.view.lblAck.text = "Error in  Submitting you Application";
    }    
  },

  /* Updating the details on the form*/
  updateFormUI : function (uiData) {
    if (uiData) {
      if (uiData.applicationSubmitResponse) {
        this.view.lblAck.text = "Application submitted successfully. Application ID for the submitted application is: "+uiData.applicationSubmitResponse[0].id;
      }
    }
  }

});