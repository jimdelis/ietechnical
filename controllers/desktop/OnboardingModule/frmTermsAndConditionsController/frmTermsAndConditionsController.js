define({ 

  /*
Invoke this method in frmTermsAndConditions form's onClick event of Next button. 
It dismisses the loadingscreen and passes the data to presentation controller
*/
  submitDetails : function(){  

    presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.showLoadingScreen();

    var onboardingModule = applicationManager.getModule("OnboardingModule");  	
    onboardingModule.presentationController.submitDetails(); 
  }

});