define({ 

  // Auto fill the data on form when clicked on info button

  /* Author name : Suman Kumar Gurram
  Auto fill the data on form when clicked on info button
  */
  autoFillFormData : function(){
    this.view.tbxFirstName.text = "Suman";
    this.view.tbxMiddleName.text = "Kumar";
    this.view.tbxLastName.text = "Gurram";
    this.view.calDateOfBirth.dateComponents = ["13", "07", "1988"];
    this.view.tbxEmail.text = "sumankumar.gurram@temenos.com";
    this.view.tbxPhoneCountryCode.text = "+91";
    this.view.tbxPhoneNumber.text = "9703459992";
    this.view.lblOccupation.text = "Software Engineer";
  },

  validateDOBEndDate  : function() {
    var todaysDate = new Date();
    var currDate = [todaysDate.getDate(), todaysDate.getMonth() +  1, todaysDate.getFullYear()];
    this.view.calDateOfBirth.validEndDate = this.view.calDateOfBirth;
  },

  /* Invoke this method onclick event of Next button. This method helps you to validate the entered email.
*/
  validateEmail: function(){
    var emailId = this.view.tbxEmail.text; 
    if(emailId === "" || emailId === null || emailId === undefined){
      this.view.lblInvalidEmailMsg.text = "Please enter the email id";
      // this.view.lblInvalidEmailMsg.skin = sknLblErrorMessage;
      this.view.lblInvalidEmailMsg.isVisible = true;
      return;
    }else{ 
      var emailRegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      var regExpValue = emailRegExp.test(emailId);
      if(regExpValue){
        this.validatePhoneNumber();
      }else{
        this.view.lblInvalidEmailMsg.text = "Invalid Email Id";
        // this.view.lblInvalidEmailMsg.skin = sknLblErrorMessage;
        this.view.lblInvalidEmailMsg.isVisible = true;
        return;
      }      
    }
  },
  /*
  * Invoke this method onBeginEditing event of Email id textbox. This method hides the error message displayed against the email id textbox
*/
  hideErrorDisplay : function(){
    if(this.view.lblInvalidEmailMsg.isVisible === true){
      this.view.lblInvalidEmailMsg.isVisible = false;
    }
  },
  /* 
 This method validates the country code and phone numbers and proceed to process the user inputs
*/

  validatePhoneNumber : function(){

    var countryCode = this.view.tbxPhoneCountryCode.text;
    var phoneNumber = this.view.tbxPhoneNumber.text;

    var countryCodeRegEx = /^(\+)([0-9]{2})$/;
    var phoneNoRegEx = /^\d{10}$/;

    var countryCodeRegExTestVal = countryCodeRegEx.test(countryCode);
    var phoneNoRegExTestVal = phoneNoRegEx.test(phoneNumber);

    if( (countryCode === "" || countryCode === null || countryCode === undefined) || (phoneNumber === "" || phoneNumber === null || phoneNumber === undefined)){
      this.view.lblInvalidPhoneNumber.text = "Country Code or Phone Number cannot be empty";
      //  this.view.lblInvalidPhoneNumber.skin = sknLblErrorMessage;
      this.view.lblInvalidPhoneNumber.isVisible = true;
    } else {
      if(countryCodeRegExTestVal){
        if(phoneNoRegExTestVal){
          this.processPersonalDetails();
        } else {
          this.view.lblInvalidPhoneNumber.text = "Invalid Phone Number !";
          // this.view.lblInvalidPhoneNumber.skin = sknLblErrorMessage;
          this.view.lblInvalidPhoneNumber.isVisible = true;
        }
      }else{
        this.view.lblInvalidPhoneNumber.text = "Invalid Country Code !";
        // this.view.lblInvalidPhoneNumber.skin = sknLblErrorMessage;
        this.view.lblInvalidPhoneNumber.isVisible = true;
      }
    } 
  },
  /*
  * Invoke this method onBeginEditing event of country and phone number textboxes.
  This method hides the error message displayed against phone number
*/
  hidePhoneNumberErrorDisplay : function(){
    if(this.view.lblInvalidPhoneNumber.isVisible === true){
      this.view.lblInvalidPhoneNumber.isVisible = false;
    }
  },

  /*
* This method helps you to calculate the age of the user based on his/her date of birth entered
*/
  calculateAge : function(birthDate) {
    birthDate = new Date(birthDate);
    var now = new Date();
    otherDate = new Date(now.getFullYear(),now.getMonth(),now.getDate());
    var years = (otherDate.getFullYear() - birthDate.getFullYear() );

    if (otherDate.getMonth() < birthDate.getMonth() || otherDate.getMonth() == birthDate.getMonth() && otherDate.getDate() < birthDate.getDate()) {
      years--;
    }
    return years;
  },

  /*
* Invoke this method on onclick event of Next button. This method allows you to collect the user inputs and passes the collected data to Onboarding module Presentation controller
*/
  processPersonalDetails : function(){  
    var personalDetails = {};

    var ageInYears = this.calculateAge(this.view.calDateOfBirth.formattedDate);

    personalDetails.firstName = this.view.tbxFirstName.text;
    personalDetails.middleName = this.view.tbxMiddleName.text; 
    personalDetails.lastName = this.view.tbxLastName.text; 
    personalDetails.dob = this.view.calDateOfBirth.formattedDate;
    personalDetails.email = this.view.tbxEmail.text; 
    personalDetails.countryCode = this.view.tbxPhoneCountryCode.text; 
    personalDetails.phoneNumber = this.view.tbxPhoneNumber.text; 
    personalDetails.occupation = this.view.lblOccupation.text; 
    personalDetails.ageInYears = ageInYears; //This is an extra field which you are passing along with collected inputs

    var presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.showLoadingScreen();

    var onboardingModule = applicationManager.getModule("OnboardingModule");  	
    onboardingModule.presentationController.processPersonalDetails(personalDetails); 
  },

  getOccupationList : function() {
    var presentationUtilMgr = applicationManager.getPresentationUtility();
    presentationUtilMgr.showLoadingScreen();

    var onboardingModule = applicationManager.getModule("OnboardingModule");  	
    onboardingModule.presentationController.getOccupationList(); 
  },

  /*
	 *
	 * This function is invoked from the updateForm function of the business controller in the navigation manager. So basically, whenever we call updateForm function from presentation controllers of any module, the updateFormUI function in that corresponding form’s controller will get called. 
     * This enabled to update only a part of a form, instead of refreshing the entire form
	 */
  updateFormUI: function(uiData) { //{"occupationListGroupedIntoSections":occupationListGroupedIntoSections}
    if (uiData) {
      if (uiData.occupationListGroupedIntoSections) {
        this.setOccupationListGroupedIntoSectionsToSegment(uiData.occupationListGroupedIntoSections);
      }
      if (uiData.occupationListErrorMessage) {
        this.setOccupationListErrorMessage(uiData.occupationListErrorMessage);
      }
    }
  },

  /* 
	 * This function is to set the occupation list response from the Java integration service to the segment
	 */
  setOccupationListGroupedIntoSectionsToSegment:function(occupationListGroupedIntoSections) {
    this.view.segOccupationList.widgetDataMap = {"lblHeading":"occupation_type","lblOccupation":"occupation"};
    this.view.segOccupationList.setData(occupationListGroupedIntoSections);
  },


  /*
	 * This function is to show the error message while invoking the Java service to fetch the occupation list
	 */
  setOccupationListErrorMessage: function(errorMessage) {
    alert ("Unable to fetch the occupation list. "+JSON.stringify(errorMessage));
  },


});