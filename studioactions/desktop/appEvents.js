define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_AppEvents_f4249f085d024aa0b9cd0e9ce2f93d23: function AS_AppEvents_f4249f085d024aa0b9cd0e9ce2f93d23(eventobject) {
        var self = this;
        /*
         * Set GlobalExceptionHandler. Load ApplicationManager and get the unique instance of ApplicationManager. Save the instance of ApplicationManager in a global variable
         */
        kony.lang.setUncaughtExceptionHandler(GlobalExceptionHandler.exceptionHandler);
        try {
            var ApplicationManager = require('ApplicationManager');
            applicationManager = ApplicationManager.getApplicationManager();
        } catch (err) {
            throw GlobalExceptionHandler.addMessageAndActionForException(err, "kony.error.App_Initialisation_Failed", GlobalExceptionHandler.ActionConstants.BLOCK, arguments.callee.name);
        }
    }
});